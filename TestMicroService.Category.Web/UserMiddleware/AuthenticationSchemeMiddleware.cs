﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using TestMicroService.Category.Web.Model;

namespace TestMicroService.Category.Web.UserMiddleware
{
    public class AuthenticationSchemeMiddleware
    {
        private readonly RequestDelegate _next;
        private IOptions<ServerAuth> serverAuth;
        public AuthenticationSchemeMiddleware(RequestDelegate next, IOptions<ServerAuth> serverAuth)
        {
            _next = next;
            this.serverAuth = serverAuth;
        }
        public async Task Invoke(HttpContext context)
        {
            if (!string.IsNullOrWhiteSpace(context.Request.Headers["AuthorizationToken"]))
            {
                HttpClient client = new HttpClient();
                var token = context.Request.Headers["AuthorizationToken"];
                using HttpResponseMessage response = await client.GetAsync( serverAuth.Value.Link + "/api/User/checkToken/" + token)
                    .ConfigureAwait(false);
                var jsonResponse =  await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var result = JsonSerializer.Deserialize<AnswerInfoRoleModel>(jsonResponse);

                if (result?.ResultCode == ResultCode.Success)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimsIdentity.DefaultRoleClaimType, result.RoleSysName),
                    };

                    var appIdentity = new ClaimsIdentity(claims, "Auth");
                    context.User = new ClaimsPrincipal(appIdentity);

                }
                else
                {
                    context.User = null;
                }

            }
            else
            {
                context.User = null;
            }

            await _next(context);
        }
    }

    public static class AuthenticationSchemeMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthenticationSchemeMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthenticationSchemeMiddleware>();
        }
    }
}
