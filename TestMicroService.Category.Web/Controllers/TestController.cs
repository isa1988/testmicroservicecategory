﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace TestMicroService.Category.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        [HttpGet("public")]
        public ActionResult GetPublicData()
        {
            return Ok("this is public data");
        }


        [Authorize(Policy = "IsUser")]
        [HttpGet("user")]
        public ActionResult GetUserData()
        {
            return Ok("this is user data");
        }


        [Authorize(Policy = "IsAdministrator")]
        [HttpGet("admin")]
        public ActionResult UpdateAdminData()
        {
            return Ok("admin has updated the data");
        }
    }
}
