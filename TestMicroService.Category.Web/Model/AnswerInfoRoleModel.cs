﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TestMicroService.Category.Web.Model
{
    public class AnswerInfoRoleModel : ErrorResponseModel
    {
        [JsonPropertyName("roleSysName")]
        public string RoleSysName { get; set; }
    }
}
