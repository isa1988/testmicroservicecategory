﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TestMicroService.Category.Web.Model
{
    public class ErrorResponseModel
    {
        [JsonPropertyName("resultCode")]
        public ResultCode ResultCode { get; set; }
        [JsonPropertyName("resultMessage")]
        public string ResultMessage { get; set; }
    }
    public enum ResultCode
    {
        Success = 0,
        Error = 1,
    }
}
